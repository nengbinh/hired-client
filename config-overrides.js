const {
    addLessLoader,
    fixBabelImports,
    override
} = require("customize-cra");

module.exports = {
    webpack: override(
        addLessLoader({
            javascriptEnabled: true,
            modifyVars: {
                "@brand-primary": "#1cae82",
                "@brand-primary-tap": "#1DA57A"
            }
        }),
        fixBabelImports("babel-plugin-import", {
            libraryName: "antd-mobile",
            style: true
        })
    )
}
