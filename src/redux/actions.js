import {
    AUTH_SUCCESS, ERROR_MSG, RECEIVE_USER, RESET_USER,
    RECEIVE_USER_LIST,
    RECEIVE_MSG_LIST, RECEIVE_MSG, MSG_READ
} from './action-types'
import {reqLogin, reqRegister, reqUpdate, reqGetuser, reqGetuserList, reqChatMsgList, reqReadMsg} from '../api'
import io from 'socket.io-client';

// socket

function initIO(dispatch, userid) {
    if(!io.socket) {
        const SOCKET = require('../setting').SOCKETIO;
        io.socket = io(SOCKET);
        //
        io.socket.on('receiveMsg', function(chatMsg){
            if(userid === chatMsg.from || userid === chatMsg.to) {
                dispatch(receive_msg(chatMsg, userid));
            }
        })
    }
}

//

async function getMsgList(dispatch, userid) {
    initIO(dispatch, userid);
    const response = await reqChatMsgList();
    const result = response.data;
    if(result.code === 0){
        const {users, chatMsgs} = result.data;
        dispatch(receiveMsgList({users, chatMsgs, userid}));
    }
}

// Synchronous action
const authSuccess = (user) => ({type: AUTH_SUCCESS, data: user});
const errorMsg = (msg) => ({type: ERROR_MSG, data: msg});
const receive_user = user => ({type: RECEIVE_USER, data: user});
const receive_user_list = user_list => ({type: RECEIVE_USER_LIST, data: user_list});
export const reset_user = msg => ({type: RESET_USER, data: msg});
const receiveMsgList = ({users, chatMsgs, userid}) => ({type:RECEIVE_MSG_LIST, data: {users, chatMsgs, userid}});
const receive_msg = (chatMsg, userid) => ({type: RECEIVE_MSG, data: {chatMsg,userid}});
const msgRead = ({count, from, to}) => ({type: MSG_READ, data: {count, from, to}});

// Async action
export const register = function(user) {
    let {username, password, password2, type} = user;
    // check form before save
    if(!username) { return errorMsg("Username can't be empty!"); }
    if(!password) { return errorMsg("Password can't be empty!"); }
    if(password !== password2) { return errorMsg("Password confirm doesn't match!"); }
    if(type !== 'candidate') { type='recruiter'; }
    //
    return async dispatch => {
        //
        const response = await reqRegister({username, password, type});
        const result = response.data;
        console.log(result);
        if(result.code === 0) {
            // success
            getMsgList(dispatch, result.data._id);
            dispatch(authSuccess(result.data));
        }else{
            // fail
            dispatch(errorMsg(result.msg));
        }
    }
};

//
export const login = function(user) {
    const {username, password} = user;
    // check form before save
    if(!username) { return errorMsg("Username can't be empty!"); }
    if(!password) { return errorMsg("Password can't be empty!"); }
    return async dispatch => {
        //
        const response = await reqLogin(user);
        const result = response.data;
        console.log(result);
        if(result.code === 0) {
            // success
            getMsgList(dispatch, result.user._id);
            dispatch(authSuccess(result.user));
        }else{
            // fail
            dispatch(errorMsg(result.msg));
        }
    }
};

//
export const update = function(user) {
    return async dispatch => {
        const response = await reqUpdate(user);
        const result = response.data;
        if(result.code === 0) {
            // suc
            dispatch(receive_user(result.data));
        }else{
            // fail
            dispatch(reset_user(result.msg));
        }
        //
    }
};

//
export const getuser = function() {
  return async dispatch => {
      const response = await reqGetuser();
      const result = response.data;
      if(result.code === 0) {
          getMsgList(dispatch, result.data._id);
          dispatch(receive_user(result.data));
      }else{
          dispatch(reset_user(result.msg));
      }
  }
};

//
export const getUserList = function(type) {
  return async dispatch => {
      const response = await reqGetuserList(type);
      const result = response.data;
      if(result.code === 0) {
          dispatch(receive_user_list(result.data));
      }else{
          dispatch(errorMsg('Unknown errors'));
      }
  }
};

//
export const sendMsg = ({from, to, content}) => {
    return dispatch => {
        io.socket.emit('sendMsg', {from, to, content});
    }
};

//
export const readMsg = (from, to) => {
  return async dispatch => {
      const response = await reqReadMsg(from);
      const result = response.data;
      if(result.code === 0) {
          const count = result.data;
          dispatch(msgRead({count,from,to}));
      }
  }
};
