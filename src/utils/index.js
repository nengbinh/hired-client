export function getRedirectTo(type, header){
    let path;
    if(type === 'recruiter') {
        path = '/recruiter'
    }else{
        path = '/candidate'
    }
    if(!header) {
        path += 'info';
    }
    return path;
}
