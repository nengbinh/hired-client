import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavBar, InputItem, Button, TextareaItem} from 'antd-mobile';
import HeaderSelector from '../../components/header-selector/header-selector';
import {update} from '../../redux/actions';
import {Redirect} from 'react-router-dom';
import Cookies from "js-cookie";

class recruiterInfo extends Component{
    state = {
        header: '',
        post: '',
        info: '',
        company: '',
        salary: ''
    };

    setHeader = (header) => {
        this.setState({
            header
        })
    };

    handleChange = (name, val) => {
        this.setState({
            [name]: val
        })
    };

    save = () => {
        const upinfo = {...this.state, userid: Cookies.get('userid')};
        this.props.update(upinfo);
    };

    render() {

        const {header, type} = this.props.user;
        if(header) {
            const path = '/' + type;
            return <Redirect to={path} />
        }

        return (
            <div>
                <NavBar>Please complete your info - Recruiter</NavBar>
                <HeaderSelector setHeader={this.setHeader} />
                <InputItem
                    placeholder='Enter your company name'
                    onChange={val => this.handleChange('company', val)}
                >Company: </InputItem>

                <InputItem
                    placeholder='Enter your hiring role'
                    onChange={val => this.handleChange('post', val)}
                >Role:</InputItem>

                <InputItem
                    placeholder='Enter salary for this role'
                    onChange={val => this.handleChange('salary', val)}
                >Salary:</InputItem>

                <TextareaItem
                    rows={3}
                    title='Expected:'
                    placeholder='Enter your expected for this role'
                    onChange={val => this.handleChange('info', val)}
                />

                <Button type='primary' onClick={this.save}>Save</Button>
            </div>
        )
    }
}

export default connect(
    state => ({user: state.user}),
    {update}
)(recruiterInfo)
