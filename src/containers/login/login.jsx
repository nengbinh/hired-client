import React, {Component} from 'react'
import Logo from '../../components/logo/logo'
import {connect} from "react-redux";
import {login} from '../../redux/actions'
import {Redirect} from 'react-router-dom';
import {
    NavBar,
    WingBlank,
    List,
    InputItem,
    WhiteSpace,
    Button
} from 'antd-mobile'


class Login extends Component{

    state = {
        username: '',
        password: '',
    };

    login = () => {
        // console.log(this.state)
        this.props.login(this.state);
    };

    handleChange = (name, val) => {
        this.setState({
            [name]: val
        })
    };

    toRegister = () => {
        this.props.user.msg = '';
        this.props.history.replace('/register');
    };

    render() {
        const {msg, redirectTo} = this.props.user;
        if(redirectTo) {
            return <Redirect to={redirectTo} />
        }
        //
        return (
            <div>
                <NavBar>Super Job Market</NavBar>
                <Logo />
                <WingBlank>
                    <List>
                        {msg ? <div className='error-msg'>{msg}</div> : null}
                        <WhiteSpace />
                        <InputItem
                            placeholder='Please enter username'
                            onChange={ val => this.handleChange('username', val) }
                        >Username:</InputItem>

                        <WhiteSpace />
                        <InputItem
                            type='password'
                            placeholder='Please enter password'
                            onChange={ val => this.handleChange('password', val) }
                        >Password:</InputItem>

                        <WhiteSpace />
                        <Button
                            type='primary'
                            onClick={this.login}
                        >Login</Button>

                        <WhiteSpace />
                        <Button
                            onClick={this.toRegister}
                        >Don't have an account?</Button>
                    </List>
                </WingBlank>
            </div>
        )
    }
}

export default connect(
    state => ({user: state.user}),
    {login}
)(Login)
