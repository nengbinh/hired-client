import React, {Component} from 'react'
import Logo from '../../components/logo/logo'
import {connect} from "react-redux";
import {register} from '../../redux/actions'
import {Redirect} from 'react-router-dom';
import {
    NavBar,
    WingBlank,
    List,
    InputItem,
    WhiteSpace,
    Radio,
    Button
} from 'antd-mobile'
const ListItem = List.Item;


class Register extends Component{
    state = {
        username: '',
        password: '',
        password2: '',
        type: 'candidate'
    };

    register = () => {
        this.props.register(this.state);
    };

    handleChange = (name, val) => {
        this.setState({
            [name]: val
        })
    };

    toLogin = () => {
        this.props.history.replace('/login');
    };

    render() {
        const {type} = this.state;
        const {msg, redirectTo} = this.props.user;
        if(redirectTo) {
            return <Redirect to={redirectTo} />
        }
        return (
            <div>
                <NavBar>Super Job Market</NavBar>
                <Logo />
                <WingBlank>
                    <List>
                        {msg ? <div className='error-msg'>{msg}</div> : null}
                        <WhiteSpace />
                        <InputItem
                            onChange={val => this.handleChange('username', val)}
                            placeholder='Please enter username'
                        >Username:</InputItem>

                        <WhiteSpace />
                        <InputItem
                            type='password'
                            onChange={val => this.handleChange('password', val)}
                            placeholder='Please enter password'
                        >Password:</InputItem>

                        <WhiteSpace />
                        <InputItem
                            type='password'
                            onChange={val => this.handleChange('password2', val)}
                            placeholder='Please confirm password'
                        >confirm:</InputItem>

                        <WhiteSpace />
                        <ListItem>
                            <span>Type:</span>
                            &nbsp;&nbsp;&nbsp;
                            <Radio
                                checked={type==='candidate'}
                                onChange={ () => this.handleChange('type', 'candidate') }
                            >Candidate</Radio>

                            &nbsp;&nbsp;&nbsp;
                            <Radio
                                checked={type==='recruiter'}
                                onChange={ () => this.handleChange('type', 'recruiter') }
                            >Recruiter</Radio>

                        </ListItem>
                        <WhiteSpace />
                        <Button type='primary' onClick={this.register}>Register</Button>
                        <WhiteSpace />
                        <Button onClick={this.toLogin}>Already have an account?</Button>
                    </List>
                </WingBlank>
            </div>
        )
    }
}

export default connect(
    state => ({user: state.user}),
    {register}
)(Register);
