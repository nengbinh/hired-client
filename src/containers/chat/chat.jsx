import React from 'react';
import {connect} from "react-redux";
import {NavBar, List, InputItem, Grid, Icon} from 'antd-mobile';
import QueueAnim from 'rc-queue-anim';
import {sendMsg, readMsg} from '../../redux/actions';
const Item = List.Item;

class Chat extends React.Component{
    state = {
        content: '',
        isShow: false
    };

    componentWillMount() {
        const emoji = ['😀', '😃', '😄','😁','😆','😅','🤣','😂','🙂','🙃','😉','😊','😇','😍','🤩','😘','😗','😚','😙','😋','😛','😜','🤪','😝','🤑','🤗','🤭','🤫','🤔','🤐','🤨','😐','😑','😶','😏','😒','🙄','😬','🤥','😌','😔','😪','🤤','😴','😷','🤒','🤕','🤢','🤮','🤧'];
        this.emojis = emoji.map(emoji => ({text: emoji}));
    }

    clearUnRead() {
        const from = this.props.match.params.userid;
        const to = this.props.user._id;
        this.props.readMsg(from, to);
    }

    componentDidMount() {
        window.scrollTo(0, document.body.scrollHeight);
        this.clearUnRead();
    }

    componentWillUnmount() {
        this.clearUnRead();
    }

    componentDidUpdate() {
        window.scrollTo(0, document.body.scrollHeight);
    }

    handleSend = () => {
        const from = this.props.user._id;
        const to = this.props.match.params.userid;
        const content = this.state.content.trim();
        if(content) {
            this.props.sendMsg({from, to, content});
        }
        this.setState({content: '', isShow: false})
    };

    toggleShow = () => {
        const isShow = !this.state.isShow;
        this.setState({isShow});
        if(isShow) {
            setTimeout(() => {
                window.dispatchEvent(new Event('resize'));
            }, 0)
        }
    };

    render() {
        const {user} = this.props;
        const {users, chatMsgs} = this.props.chat;
        //
        const meId = user._id;
        if(!users[meId]) {
            return null;
        }
        const targetId = this.props.match.params.userid;
        const chatId = [meId, targetId].sort().join('_');
        //
        const msgs = chatMsgs.filter(msg=>msg.chat_id === chatId);
        // target & me icon
        const targetHeader = users[targetId].header;
        const targetIcon = targetHeader ? require(`../../assets/images/${targetHeader}.png`) : null;
        const myIcon = require(`../../assets/images/${user.header}.png`);
        //
        return (
            <div id='chat_page'>
                <NavBar
                    icon={<Icon type='left' />}
                    className='sticky-header'
                    onLeftClick={()=>this.props.history.goBack()}
                >
                    {users[targetId].username}
                </NavBar>

                <List style={{marginTop: 50, marginBottom: 44}}>
                    <QueueAnim type='left'>
                        {
                            msgs.map( msg => {
                                if(targetId === msg.from) {
                                    return <Item key={msg._id} thumb={targetIcon}>{msg.content}</Item>;
                                }else{
                                    return ( <Item key={msg._id}
                                                   className='chat-me'
                                                   extra={<img alt='.' src={myIcon} />}>
                                        {msg.content}</Item> );
                                }
                            })
                        }
                    </QueueAnim>
                </List>

                <div className='am-tab-bar'>
                    <InputItem
                        onFocus={() => this.setState({isShow: false})}
                        placeholder='text...'
                        value={this.state.content}
                        onChange={val => this.setState({content: val})}
                        extra={
                            <div>
                                <span role='img' aria-label='emoji' onClick={this.toggleShow}>😉</span>
                                <span onClick={this.handleSend}>Send</span>
                            </div>
                        }
                    />
                {
                    this.state.isShow ? (
                        <Grid data={this.emojis}
                              columnNum={8}
                              carouselMaxRow={4}
                              isCarousel={true}
                              onClick={ (item) => {
                                  this.setState({content: this.state.content + item.text})
                              }}/>
                    ) : null
                }
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({user: state.user, chat: state.chat}),
    {sendMsg, readMsg}
)(Chat)
