import React, {Component} from 'react';
import {connect} from "react-redux";
import {List, Badge} from "antd-mobile";
const Item = List.Item;
const Brief = Item.Brief;

function getLastMsgs(chatMsgs, userid) {
    // container
    const lastMsgObjs = {};
    // 1. get each chat's last msg and save it in a container
    chatMsgs.forEach(msg => {
        //
        if(msg.to === userid && !msg.read){
            msg.unReadCount = 1;
        }else{
            msg.unReadCount = 0;
        }
        //
        const chatId = msg.chat_id;
        let lastMsg = lastMsgObjs[chatId];
        //
        if(!lastMsg) {
            lastMsgObjs[chatId] = msg
        }else{
            const unReadCount = lastMsg.unReadCount + msg.unReadCount;
            if(msg.creat_time > lastMsg.creat_time){
                lastMsgObjs[chatId] = msg
            }
            lastMsgObjs[chatId].unReadCount = unReadCount;
        }
    });
    // 2. get all the lastmsg's array
    const lastMsgs = Object.values(lastMsgObjs);

    // 3. array sort by creat_time
    lastMsgs.sort(function(m1, m2){
        return m2.creat_time - m1.creat_time;
    });

    return lastMsgs;
}

class Messages extends Component{
    render(){

        const {user} = this.props;
        const {users, chatMsgs} = this.props.chat;
        //
        const lastMsgs = getLastMsgs(chatMsgs, user._id);
        return(
            <List style={{marginBottom: 50, marginTop: 50}}>
                {
                    lastMsgs.map( msg => {
                        const targetUserId = msg.to === user._id ? msg.from : msg.to;
                        const targetUser = users[targetUserId];
                        return (
                            <Item key={msg._id}
                                  extra={<Badge text={msg.unReadCount} />}
                                  thumb={require(`../../assets/images/${targetUser.header}.png`)}
                                  arrow='horizontal'
                                  onClick={ () => this.props.history.push(`/chat/${targetUserId}`) }
                            >
                                {msg.content}
                                <Brief>User: {users[msg.to===user._id?msg.from:msg.to].username}</Brief>
                            </Item>
                        )
                    } )
                }
            </List>
        )
    }
}

export default connect(
    state => ({user: state.user, chat: state.chat}),
    {}
)(Messages)
