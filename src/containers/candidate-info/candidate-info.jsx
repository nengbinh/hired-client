import React, {Component} from 'react';
import {connect} from "react-redux";
import {Redirect} from 'react-router-dom';
import {NavBar, InputItem, Button, TextareaItem} from 'antd-mobile';
import HeaderSelector from '../../components/header-selector/header-selector';
import {update} from "../../redux/actions";

class candidateInfo extends Component{

    state = {
        header: '',
        post: '',
        info: ''
    };

    setHeader = (header) => {
        this.setState({
            header
        })
    };

    handleChange = (name, val) => {
        this.setState({
            [name]: val
        })
    };

    save = () => {
        if(this.state.header === '') {
            this.setState({header: 'Avatar 1'})
        }
        this.props.update(this.state);
    };

    render() {
        const {header, type} = this.props.user;
        if(header) {
            const path = '/' + type;
            return <Redirect to={path} />
        }

        return (
            <div>
                <NavBar>Please complete your info - Candidate</NavBar>
                <HeaderSelector setHeader={this.setHeader} />
                <InputItem
                    placeholder='Enter role that your looking for'
                    onChange={ val => this.handleChange('post', val)}
                >Role:</InputItem>

                <TextareaItem
                    rows={3}
                    title='Summary:'
                    placeholder='Enter a short summary'
                    onChange={ val => this.handleChange('info', val)}
                />

                <Button type='primary' onClick={this.save}>Save</Button>
            </div>
        )
    }
}

export default connect(
    state => ({user: state.user}),
    {update}
)(candidateInfo)
