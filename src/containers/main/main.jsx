import React, {Component} from 'react'
import {Switch, Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import Cookies from 'js-cookie'
import {getRedirectTo} from '../../utils';
import {getuser} from '../../redux/actions'

import recruiterInfo from '../recruiter-info/recruiter-info';
import candidateInfo from '../candidate-info/candidate-info';
import recruiter from '../recruiter/recruiter';
import candidate from '../candidate/candidates';
import personal from '../personal/personal';
import message from '../message/message';
import notFound from '../../components/not-found/notFound'
import {NavBar} from "antd-mobile";
import Footer from '../../components/footer/footer'
import Chat from '../chat/chat';

class Main extends Component{

    componentDidMount() {
        const userid = Cookies.get('userid');
        const {_id} = this.props.user;
        if(userid && !_id) {
            // send ajax
            this.props.getuser();
        }
    }

    navList = [
        {
            path: '/recruiter',
            component: recruiter,
            title: 'Candidates list',
            icon: 'candidates',
            text: 'Candidates'
        },
        {
            path: '/candidate',
            component: candidate,
            title: 'Recruiters list',
            icon: 'recruiters',
            text: 'Recruiters'
        },
        {
            path: '/message',
            component: message,
            title: 'Messages',
            icon: 'message',
            text: 'Messages'
        },
        {
            path: '/personal',
            component: personal,
            title: 'Personal information',
            icon: 'personal',
            text: 'Personal'
        },
    ];

    render() {
        //
        const userid = Cookies.get('userid');
        const {user, unReadCount} = this.props;
        // still not exist
        if(!userid) {
            return <Redirect to='/login' />
        }
        //
        if(!user._id) {
            return null;
        }else{
            let path = this.props.location.pathname;
            if(path === '/') {
                path = getRedirectTo(user.type, user.header);
                return <Redirect to={path} />
            }
        }
        //
        const {navList} = this;
        const path = this.props.location.pathname;
        const currentNav = navList.find(nav => nav.path === path);

        if(currentNav) {
            if(user.type === 'candidate') {
                navList[0].hide = true;
            }else{
                navList[1].hide = true;
            }
        }

        return (
            <div>
                {currentNav ? <NavBar className='sticky-header'>{currentNav.title}</NavBar> : null}
                <Switch>
                    {
                        navList.map(nav => <Route key={nav.path} path={nav.path} component={nav.component} /> )
                    }
                    <Route path='/recruiterinfo' component={recruiterInfo} />
                    <Route path='/candidateinfo' component={candidateInfo} />
                    <Route path='/chat/:userid' component={Chat} />

                    <Route component={notFound} />
                </Switch>
                {currentNav ? <Footer navList={navList} unReadCount={unReadCount} /> : null }
            </div>
        )
    }
}

export default connect(
    state => ({user: state.user, unReadCount: state.chat.unReadCount}),
    {getuser}
)(Main)
