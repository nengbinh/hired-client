import React, {Component} from 'react';
import {connect} from "react-redux";
import {Result, List, WhiteSpace, Button, Modal} from 'antd-mobile';
import Cookies from 'js-cookie';
import {reset_user} from '../../redux/actions';

const Item = List.Item;
const Brief = Item.Brief;

class Personal extends Component{

    Logout = () => {
        Modal.alert('Sign Out','Are you sure to Sign Out?', [
            {text: 'No'},
            {
                text: 'Yes',
                onPress: () => {
                    Cookies.remove('userid');
                    this.props.reset_user();
                }
            }
        ])
    };

    render(){
        const {user} = this.props;
        return(
            <div style={{marginTop: 45}}>
                <Result img={<img src={require(`../../assets/images/${user.header}.png`)}
                                  style={{width: 50}}
                                  alt='header' />}
                        title={user.username}
                        message= {
                            user.type === 'candidate' ?
                                'Summary: ' + user.info
                                :
                                user.company
                        }

                />
                <List renderHeader={() => {
                    return user.type === 'candidate' ?
                        'Seeking for job'
                        :
                        'Looking for candidates'
                }}>
                    {
                        user.type === 'candidate' ?
                            <Item multipleLine>
                                <Brief>Role: {user.post}</Brief>
                            </Item>
                            :
                            <Item multipleLine>
                                <Brief>Role: {user.post}</Brief>
                                <Brief>Salary: {user.salary}</Brief>
                                <Brief>Expect: {user.info}</Brief>
                            </Item>
                    }
                </List>
                <WhiteSpace />
                <List>
                    <Button type='warning' onClick={this.Logout}>Sign Out</Button>
                </List>
            </div>
        )
    }
}

export default connect(
    state => ({user: state.user}),
    {reset_user}
)(Personal)
