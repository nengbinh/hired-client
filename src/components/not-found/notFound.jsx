import React from 'react';
import {Button} from 'antd-mobile';

export default class NotFound extends React.Component{
    render() {
        return(
            <div>
                <div>
                    <h2>Sorry, Page not found!</h2>
                    <Button type='primary' onclick={this.props.history.replace('/')}>
                        Back to home page
                    </Button>
                </div>
            </div>
        )
    }
}
