import React from 'react';
import {Card, WingBlank, WhiteSpace} from "antd-mobile";
import PropTypes from 'prop-types';
import QueueAnim from 'rc-queue-anim';
import './style.less';
import {withRouter} from 'react-router-dom';
const {Header, Body} = Card;

class UserList extends React.Component{
    static propTypes = {
        userList: PropTypes.array.isRequired
    };

    render() {
        const {userList} = this.props;
        return (
            <WingBlank style={{marginTop: 45, marginBottom: 50}}>
                <QueueAnim type='scale'>
                    {
                        userList.map( user => (
                            <div key={user._id}>
                                <WhiteSpace />
                                <Card className='user-list'
                                      onClick={() => this.props.history.push(`/chat/${user._id}`)}
                                >
                                    <Header thumb={require(`../../assets/images/${user.header}.png`)}
                                            extra={user.username} />
                                    <Body>
                                    <div> <span className='title'>Role:</span>
                                        <span className='text'>{user.post}</span></div>
                                    {user.company
                                        ? <div><span className='title'>Company:</span>
                                            <span className='text'>{user.company}</span></div>
                                        : null}
                                    {user.salary
                                        ? <div><span className='title'>Salary:</span>
                                            <span className='text'>{user.salary}</span></div>
                                        : null}
                                    {user.info
                                        ? <div><span className='title'>Expect:</span>
                                               <span className='text'>{user.info}</span></div>
                                        : null}
                                    </Body>
                                </Card>
                            </div>
                        ))
                    }
                </QueueAnim>
            </WingBlank>
        )
    }
}

export default withRouter(UserList);
