import React, {Component} from 'react';
import {List, Grid} from 'antd-mobile';
import PropTypes from 'prop-types';

export default class HeaderSelector extends Component{

    constructor(props) {
        super(props);
        this.headerList = [];
        for(let i=1; i<=20; i++) {
            this.headerList.push({
                text: 'Avator ' + i,
                icon: require(`../../assets/images/Avator ${i}.png`)
            })
        }
    }

    state = {
        icon: null
    };

    static propTypes = {
        setHeader: PropTypes.func.isRequired
    };

    handleClick = ({text,icon}) => {
        this.setState({icon});
        this.props.setHeader(text);
    };

    render() {
        const {icon} = this.state;
        const listHeader = () => {
            return !icon ? 'Select your avatar' : (
                <div>
                    Selected: <img alt='header' src={icon}/>
                </div>
            );
        };

        return (
            <List renderHeader={listHeader}>
                <Grid
                    data={this.headerList}
                    columnNum={5}
                    onClick={this.handleClick}
                />
            </List>
        )
    }
}
