import ajax from './ajax'

const proxy = require('../setting').SERVER;

// Register API
export const reqRegister = user => ajax(proxy + '/register', user, 'post');

// Login API
export const reqLogin = user => ajax(proxy + '/login', user, 'post');

// Update API
export const reqUpdate = user => ajax(proxy + '/update', user, 'post');

// get user API
export const reqGetuser = () => ajax(proxy + '/user');

// get user list
export const reqGetuserList = (type) => ajax(proxy + '/userlist', {type});

// get chat list
export const reqChatMsgList = () => ajax(proxy + '/msglist');

// set read msg
export const reqReadMsg = from => ajax(proxy + '/readmsg', {from}, 'post');
