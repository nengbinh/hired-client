const axios = require('axios');

export default function ajax(url, data={}, type='get'){
    if(type === 'get'){
        let paramStr = '';
        Object.keys(data).forEach(key => {
            paramStr += key + '=' + data[key] + '&';
        });
        if(paramStr) {
            paramStr.substr(0, paramStr.length -1 );
        }
        return axios.get(url + '?' + paramStr, {withCredentials: true});
    }else{
        return axios.post(url, data, {withCredentials: true});
    }
}
